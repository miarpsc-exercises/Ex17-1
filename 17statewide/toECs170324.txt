OK, yet another long, rambling email from the SEC.

As we look at the potential for a long term power outage, a couple of
key things manifest themselves:

- Beyond day 4 or 5 we are very likely to become critical
- If we do, it will be hard and we may not be ready

First things first; family

We won't be of any use to anyone if we can't take care of ourselves
and our family.  We all know, water, food, medication, generator fuel,
vehicle fuel for 72 hours.  Keep the gas tank full, keep the cell phone
charged. How many of us actually do this?  We all know we should, but
do we?  What if it goes beyond 72 hours.  One sobering recognition
from the recent scenarios is that if the incident is sufficiently
widespread, resources from FEMA might not be readily available.  FEMA
can get to a concentrated incident in 72 hours, but if a large part of
the upper Midwest is out of power, there just aren't enough resources
to get to everyone in that short a time. We should be thinking hard
about lasting well beyond 72 hours.

It is suspected that after 72 hours most EOCs won't have power.
Probably each EC should understand their county's situation with
respect to emergency power and fuel on hand. We also might begin
loosing some MPSCS towers.  Most should be good for far longer, but
many have propane, and the length of time they survive is dependent on
when they last got fueled. In some cases a single tower can isolate a
large part of the system, although that isn't nearly as likely as it
used to be.

And what about our response? How do we operate without power? We have
some assets that can help if we think about it.

In most cases, natural gas should be available. A lot of hospitals
have natural gas generators, and in a lot of cases we have antennas on
those hospitals.  Do we KNOW (not guess) that our hospital has natural
gas? Can we access the end of that feedline so we can drag a mobile up
there?  Can we charge our HTs there?

What about HF? One major need from the field will be getting resource
requests into the SEOC from the counties.  Unless you are close, that
is going to take HF.  Fortunately, with the new SEOC "close" is a much
bigger footprint than it used to be, but at this point we don't really
know what that is.

And these resource requests will be fairly complex. Don't expect
anything to fit into a 25 word radiogram.  *BUT*, be sure you have on
your staff at least a couple of skilled traffic handlers.  People with
no practice just aren't going to be able to get these complex messages
through.  We ran that experiment a few SETs back. Even better if you
have a skilled CW traffic handler; CW still does a lot better. Of
course NBEMS where possible, but not always possible, and an
additional drain on scarce power. Think about identifying a traffic op
with a strong home station that can relay for you. Even if MPSCS is
up, that system wasn't designed for complex messages. A skilled
traffic handler at the mike of an MPSCS radio could be a critical
asset in some situations.

Something else you probably don't think of.  I suspect almost every
county has one or two stations that run off solar or wind. Odds are
that guy isn't an ARES member, or even a club member.  Probably we
don't know who it is.  Perhaps it is time to make a new friend.  Ditto
with that guy who hoards generator fuel.  They won't be of any help if
we don't know who they are.

Which brings us to the ugliest part.  We really should at least
discuss these responses in ARES meetings.  Your members will come up
with good ideas and solutions.  When possible we should exercise these
scenarios.  But, and this is the ugly part, we need to DOCUMENT these
plans.  We probably should have a written plan for any likely
incident, and a more general plan for those we didn't think of.  But
solutions we discovered in exercises and tabletops will get lost if we
don't write them down.

In talking to folks over the past few weeks, I realized that there are
some details about a cyber attack on the grid that a lot of folks
didn't know or perhaps just hadn't connected the dots.

If we had a generating station with a virus, we can't just start it
up. We need to disinfect the control system first.  These aren't like
a single laptop; these are complex systems with lots of
processors. They need to be isolated first, then disinfected, then
connected to simulated inputs and tested before being put back
online.  The Michigan Cyber Range provides this capability, but with
limited capacity.  If dozens of plants are hit, it will take some time
to get them ready to restart, in addition to repairing any physical
damage that may have been done.

But it doesn't stop there.  If the grid is down, a plant can't just
restart. It needs to be connected to the grid.  There are a handful of
"dark start" plants that can start without the grid. Their number and
locations is a closely held secret for obvious reasons.  Once they are
started then adjacent plants can be started, then adjacent to them,
etc.  So just because you have a generating plant in your county it
doesn't mean they can just start sending you electricity.

So if you loose the grid entirely, even in a good situation it could
take days to get back online, and with a widespread cyber attack,
weeks would be more likely.

Pretty grim I know.  But we can mitigate the difficulty if we have
thought out responses ahead of time, and this is a good topic for
discussion among your group.

I mentioned to the Region V Auxcomm group that FEMA is paranoid about
a serious attack.  The RECC countered that you are only paranoid if
you think they are out to get you.  We KNOW they are out to get us. He
like the term "defensively-postured".  Nice weasel words I think.

73 de WB8RCR

