#include <stdio.h>
#include <string.h>

#define FILENAME "NIMS_Resource_Typing_prod_20170530_.json"

char szBuffer[256];
char szResource[256];


int main( void )
{
    FILE *f;
    char *p,*q;

    f = fopen(FILENAME,"r");
    if ( f==NULL )
	{
	    perror("Opening");
	    return 0;
	}
    while ( !feof(f) )
	{
	    fgets(szBuffer,sizeof(szBuffer),f);
	    if ( strstr(szBuffer,"\"name\"") )
		{
		    p = strchr(szBuffer,'"')+8;
		    if ( (p-szBuffer)<12 )
			printf("-------------------\n");
		    q = strchr(p,'"')+1;
		    p = strchr(q,'"');
		    *p = '\0';
		    if ( strncmp("Type ",q,5) )
			{
			    printf("	      <option value=\"%s\">%s</option>\n",q,q);
			}
		}
	}
    return 1;
}
