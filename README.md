# Spring 2017 Exercise

This year the Statewide 2017 exercise will be used as the Spring exercise

The following counties are scheduled to play:

* Alcona
* Arenac
* Bay
* Cheboygan
* Clinton
* Emmet
* Genesee
* Gladwin
* Houghton
* Lapeer
* Luce
* Marquette
* Midland
* Ogemaw
* Ontonagon
* Saginaw
* Tuscola
* Wayne

The MSEL is being developed by the State Training and Exercise 
department, so there is relatively little in this repository
